Run with docker  

`$ docker-compose up -d --build`

Generate database with

`$ docker-compose exec web flask db upgrade`

Running tests with

`$ docker-compose exec web nosetests`