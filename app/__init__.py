import os
from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from celery import Celery

db = SQLAlchemy()
migrate = Migrate()

CELERY_TASK_LIST = ["app.helper.tasks"]


def create_celery_app(app):
    celery = Celery(
        app.import_name,
        broker=app.config["CELERY_BROKER_URL"],
        include=CELERY_TASK_LIST,
    )
    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


def create_app(script_info=None):
    app = Flask(__name__, static_folder="assets", template_folder="views")
    app_settings = os.environ.get("APP_CONFIG")
    app.config.from_object(app_settings)

    db.init_app(app)
    migrate.init_app(app, db)
    app.app_context().push()

    from app.controllers.email_controller import email
    from app.controllers.recipient_controller import recipient

    app.register_blueprint(email)
    app.register_blueprint(recipient)

    @app.shell_context_processor
    def ctx():
        return {"app": app, "db": db}

    return app
