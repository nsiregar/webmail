from dateutil import parser
from pytz import timezone
from schema import Schema
from sqlalchemy import exc
from app import db
from flask import Blueprint, request, jsonify, current_app
from app.models.event import Event


email = Blueprint("email", __name__)


@email.route("/get_emails", methods=["GET"])
def get_emails():
    response_object = {
        "status": "success",
        "data": {"emails": [email.to_json() for email in Event.query.all()]},
    }
    return jsonify(response_object), 200


@email.route("/get_emails/<id>", methods=["GET"])
def get_email(id):
    response_object = {"status": "fail", "message": "Email does not exist"}
    try:
        event = Event.query.filter_by(event_id=int(id)).first()
        if not event:
            return jsonify(response_object), 404
        response_object = {"status": "success", "data": dict(event.to_json())}
        return jsonify(response_object), 200
    except ValueError:
        return jsonify(response_object), 404


@email.route("/save_emails", methods=["POST"])
def save_emails():
    event_schema = Schema(
        {"event_id": int, "email_subject": str, "email_content": str, "timestamp": str}
    )
    tz = timezone(current_app.config["TIMEZONE"])
    data = request.get_json()
    response_object = {"status": "fail", "message": "Invalid payload"}
    if not event_schema.is_valid(data):
        return jsonify(response_object), 400
    try:
        event_id = data.get("event_id")
        event = Event.query.filter_by(event_id=event_id).first()
        if not event:
            e = Event(event_id=event_id)
            e.email_subject = data.get("email_subject")
            e.email_content = data.get("email_content")
            e.timestamp = tz.localize(parser.parse(data.get("timestamp"), ignoretz=True))
            db.session.add(e)
            db.session.commit()

            from app.helper.tasks import send_email
            from app.models.recipient import Recipient

            recipients = Recipient.query.filter_by(active=True).all()
            for recipient in recipients:
                send_email.apply_async((e.event_id, recipient.email), eta=e.timestamp)

            response_object["status"] = "success"
            response_object["message"] = f"email subject {e.email_subject} was added!"
            return jsonify(response_object), 201
        else:
            response_object["message"] = f"email with event id {event_id} already exist"
            return jsonify(response_object), 400
    except exc.IntegrityError as error:
        db.session.rollback()
        response_object["message"] = error
        return jsonify(response_object), 400
