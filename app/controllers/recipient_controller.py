from app import db
from flask import Blueprint, request, jsonify
from app.models.recipient import Recipient
from schema import Schema
from sqlalchemy import exc

recipient = Blueprint("recipient", __name__)


@recipient.route("/get_recipients", methods=["GET"])
def get_recipients():
    response_object = {
        "status": "success",
        "data": {
            "recipients": [recipient.to_json() for recipient in Recipient.query.all()]
        },
    }
    return jsonify(response_object), 200


@recipient.route("/get_recipients/<int:id>", methods=["GET"])
def get_recipient(id):
    response_object = {"status": "fail", "message": "Recipient does not exist"}
    try:
        recipient = Recipient.query.filter_by(id=int(id)).first()
        if not recipient:
            return jsonify(response_object), 404
        response_object = {"status": "success", "data": dict(recipient.to_json())}
        return jsonify(response_object), 200
    except ValueError:
        return jsonify(response_object), 404


@recipient.route("/save_recipients", methods=["POST"])
def save_recipients():
    recipient_schema = Schema({"first_name": str, "last_name": str, "email": str})
    data = request.get_json()
    response_object = {"status": "fail", "message": "Invalid payload"}
    if not recipient_schema.is_valid(data):
        return jsonify(response_object), 400
    try:
        recipient_email = data.get("email")
        recipient = Recipient.query.filter_by(email=recipient_email).first()
        if not recipient:
            r = Recipient()
            r.first_name = data.get("first_name")
            r.last_name = data.get("last_name")
            r.email = data.get("email")
            db.session.add(r)
            db.session.commit()
            response_object["status"] = "success"
            response_object["message"] = f"Recipient {r.email} was added!"
            return jsonify(response_object), 201
        response_object["message"] = f"Recipient with {recipient_email} already exist"
        return jsonify(response_object), 400
    except exc.IntegrityError as error:
        db.session.rollback()
        response_object["message"] = error
        return jsonify(response_object), 400


@recipient.route("/update_recipients/<int:id>", methods=["PUT"])
def update_recipients(id):
    pass


@recipient.route("/delete_recipients/<int:id>", methods=["DELETE"])
def delete_recipients(id):
    pass
