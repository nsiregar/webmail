from flask import jsonify


def sendmail(recipient, subject, content):
    data = {"to": recipient, "subject": subject, "content": content}
    return jsonify(data)
