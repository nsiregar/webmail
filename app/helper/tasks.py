from app import create_celery_app, create_app
from app.models.recipient import Recipient
from app.models.event import Event
from app.helper.mailer import sendmail

app = create_app()
celery = create_celery_app(app)


@celery.task()
def send_email(event_id, email):
    event = Event.query.get(int(event_id))
    sendmail(email, event.email_subject, event.email_content)

    return None
