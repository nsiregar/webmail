from app import db
from dateutil.tz import gettz
from datetime import datetime
from config.application import BaseConfig

timezone = BaseConfig.TIMEZONE


class Event(db.Model):
    event_id = db.Column(db.Integer, primary_key=True)
    email_subject = db.Column(db.String(120), nullable=False)
    email_content = db.Column(db.String(200), nullable=False)
    timestamp = db.Column(db.DateTime, default=datetime.now(gettz(timezone)))

    def to_json(self):
        return {
            "event_id": self.event_id,
            "email_subject": self.email_subject,
            "email_content": self.email_content,
            "timestamp": self.timestamp,
        }
