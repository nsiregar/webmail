from app import db


class Recipient(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(120), nullable=False)
    last_name = db.Column(db.String(120), nullable=False)
    email = db.Column(db.String(200), unique=True, index=True)
    active = db.Column(db.Boolean, default=True)

    def __repr__(self):
        return "Recipient {}".format(self.email)

    def to_json(self):
        return {
            "id": self.id,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "email": self.email,
            "active": self.active,
        }
