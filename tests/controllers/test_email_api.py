import json
import unittest
from app import db
from app.models.event import Event

from tests.base import BaseTestCase


class TestEmailServices(BaseTestCase):
    def test_save_emails(self):
        with self.client:
            response = self.client.post(
                "/save_emails",
                data=json.dumps(
                    {
                        "event_id": 1,
                        "email_subject": "Test Email Subject",
                        "email_content": "Test Email Content",
                        "timestamp": "15 Dec 2015 23:12",
                    }
                ),
                content_type="application/json",
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 201)
            self.assertIn("email subject Test Email Subject was added", data["message"])
            self.assertIn("success", data["status"])

    def test_save_emails_invalid_json(self):
        with self.client:
            response = self.client.post(
                "/save_emails", data=json.dumps({}), content_type="application/json"
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn("Invalid payload", data["message"])
            self.assertIn("fail", data["status"])

    def test_save_emails_invalid_json_keys(self):
        with self.client:
            response = self.client.post(
                "/save_emails",
                data=json.dumps({"email_subject": "Test Invalid Key Subject"}),
                content_type="application/json",
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn("Invalid payload", data["message"])
            self.assertIn("fail", data["status"])

    def test_save_emails_duplicate_event(self):
        with self.client:
            self.client.post(
                "/save_emails",
                data=json.dumps(
                    {
                        "event_id": 1,
                        "email_subject": "Test Email Subject #1",
                        "email_content": "Test Email Content #1",
                        "timestamp": "15 Dec 2015 23:12",
                    }
                ),
                content_type="application/json",
            )
            response = self.client.post(
                "/save_emails",
                data=json.dumps(
                    {
                        "event_id": 1,
                        "email_subject": "Test Email Subject #2",
                        "email_content": "Test Email Content #2",
                        "timestamp": "15 Dec 2015 23:12",
                    }
                ),
                content_type="application/json",
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn("email with event id 1 already exist", data["message"])
            self.assertIn("fail", data["status"])

    def test_save_emails_in_unicode_compliant(self):
        with self.client:
            response = self.client.post(
                "/save_emails",
                data=json.dumps(
                    {
                        "event_id": 1,
                        "email_subject": "Test Email Unicode",
                        "email_content": "U+00FE ð",
                        "timestamp": "15 Dec 2015 23:12",
                    }
                ),
                content_type="application/json",
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 201)
            self.assertIn("email subject Test Email Unicode was added", data["message"])
            self.assertIn("success", data["status"])

    def test_get_single_email(self):
        event = Event()
        event.event_id = 1
        event.email_subject = "Test Get Single Email"
        event.email_content = "Content from single email"
        db.session.add(event)
        db.session.commit()
        with self.client:
            response = self.client.get(f"/get_emails/{event.event_id}")
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertIn("Test Get Single Email", data["data"]["email_subject"])
            self.assertIn("Content from single email", data["data"]["email_content"])
            self.assertIn("success", data["status"])

    def test_get_single_email_incorrect_id(self):
        with self.client:
            response = self.client.get("/get_emails/101")
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertIn("Email does not exist", data["message"])
            self.assertIn("fail", data["status"])

    def test_get_single_email_no_id(self):
        with self.client:
            response = self.client.get("/get_emails/test")
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertIn("Email does not exist", data["message"])
            self.assertIn("fail", data["status"])

    def test_get_all_emails(self):
        event_1 = Event(
            event_id=1,
            email_subject="Test Email #1",
            email_content="Content email #1",
            timestamp="20 Dec 2018 23:20",
        )
        event_2 = Event(
            event_id=2,
            email_subject="Test Email #2",
            email_content="Content email #2",
            timestamp="21 Dec 2018 07:00",
        )
        db.session.add(event_1)
        db.session.add(event_2)
        db.session.commit()
        with self.client:
            response = self.client.get("/get_emails")
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(data["data"]["emails"]), 2)
            self.assertIn("Test Email #1", data["data"]["emails"][0]["email_subject"])
            self.assertIn("Test Email #2", data["data"]["emails"][1]["email_subject"])


if __name__ == "__main__":
    unittest.main()
