import json
import unittest
from app import db
from app.models.recipient import Recipient

from tests.base import BaseTestCase


class TestRecipientServices(BaseTestCase):
    def test_save_recipients(self):
        with self.client:
            response = self.client.post(
                "/save_recipients",
                data=json.dumps(
                    {
                        "first_name": "Johny",
                        "last_name": "English",
                        "email": "johny@english.com",
                    }
                ),
                content_type="application/json",
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 201)
            self.assertIn("Recipient johny@english.com was added!", data["message"])
            self.assertIn("success", data["status"])

    def test_save_recipients_invalid_json(self):
        with self.client:
            response = self.client.post(
                "/save_recipients", data=json.dumps({}), content_type="application/json"
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn("Invalid payload", data["message"])
            self.assertIn("fail", data["status"])

    def test_save_recipients_invalid_json_keys(self):
        with self.client:
            response = self.client.post(
                "/save_recipients",
                data=json.dumps({"last_name": "English"}),
                content_type="application/json",
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn("Invalid payload", data["message"])
            self.assertIn("fail", data["status"])

    def test_save_recipients_duplicate_emails(self):
        with self.client:
            self.client.post(
                "/save_recipients",
                data=json.dumps(
                    {
                        "first_name": "Johny",
                        "last_name": "English",
                        "email": "johny@english.com",
                    }
                ),
                content_type="application/json",
            )
            response = self.client.post(
                "/save_recipients",
                data=json.dumps(
                    {
                        "first_name": "English",
                        "last_name": "Johny",
                        "email": "johny@english.com",
                    }
                ),
                content_type="application/json",
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn(
                "Recipient with johny@english.com already exist", data["message"]
            )
            self.assertIn("fail", data["status"])
