import os
import unittest

from flask import current_app
from flask_testing import TestCase
from app import create_app

app = create_app()


class TestDevelopmentConfig(TestCase):
    def create_app(self):
        app.config.from_object("config.application.DevelopmentConfig")
        return app

    def test_app_is_development(self):
        self.assertTrue(app.config["DEBUG"])
        self.assertTrue(
            app.config["SQLALCHEMY_DATABASE_URI"] == os.environ.get("DATABASE_URL_DEV")
        )


class TestTestingConfig(TestCase):
    def create_app(self):
        app.config.from_object("config.application.TestingConfig")
        return app

    def test_app_is_testing(self):
        self.assertTrue(app.config["TESTING"])
        self.assertTrue(app.config["DEBUG"])
        self.assertTrue(
            app.config["SQLALCHEMY_DATABASE_URI"] == os.environ.get("DATABASE_URL_TEST")
        )


class TestProductionConfig(TestCase):
    def create_app(self):
        app.config.from_object("config.application.ProductionConfig")
        return app

    def test_app_is_production(self):
        self.assertFalse(app.config["TESTING"])
        self.assertFalse(app.config["DEBUG"])
        self.assertTrue(
            app.config["SQLALCHEMY_DATABASE_URI"] == os.environ.get("DATABASE_URL_PROD")
        )


if __name__ == "__main__":
    unittest.main()
